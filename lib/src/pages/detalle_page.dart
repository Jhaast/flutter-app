import 'package:flutter/material.dart';
import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:quick/src/bloc/comentarios_bloc.dart';
import 'package:quick/src/models/comentarios_model.dart';
import 'package:quick/src/models/registros_model.dart';

class DetallePage extends StatelessWidget {
  ComentariosBloc comentarios;
  @override
  Widget build(BuildContext context) {
    final Registro registro = ModalRoute.of(context).settings.arguments;
    comentarios = BlocProvider.of(context);
    comentarios.getComentarios(registro.id);

    return Scaffold(
      appBar: AppBar(
        title: Text('Detalles'),
      ),
      body: StreamBuilder(
            stream: comentarios.comentariosStream,
            initialData: null,
            builder:
            (BuildContext context, AsyncSnapshot<List<Comentario>> snapshot) {
              if (snapshot.hasData) { 
                if(snapshot.data.length < 1 || snapshot.data == null){
                  return Center(
                    child: Text('No hay comentarios'),
                  );
                }
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, i) =>
                        _comentarios(context, snapshot.data[i]));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        );
      }

  Widget _comentarios(BuildContext context, Comentario data) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          children: <Widget>[
            SizedBox(width: 20.0),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(data.name,
                      style: Theme.of(context).textTheme.headline6),
                  Text(
                    data.email,
                    style: TextStyle(color: Colors.blueAccent),
                  ),
                  Text(data.body, style: Theme.of(context).textTheme.subtitle2),
                  Divider()
                ],
              ),
            )
          ],
        ),
      ),
      onTap: (){
        Navigator.pushNamed(context, 'detalles', arguments: data);
      },
    );
  }
}