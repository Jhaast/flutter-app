import 'dart:async';

import 'package:generic_bloc_provider/generic_bloc_provider.dart';
import 'package:quick/src/models/registros_model.dart';
import 'package:quick/src/providers/registros_provider.dart';

class RegistrosBloc extends Bloc {
  final _registrosController = StreamController<List<Registro>>.broadcast();

  // Recuperar los datos del Stream
  Stream<List<Registro>> get registrosStream => _registrosController.stream;

  // Insertar valores al Stream
  Function(List<Registro>) get changeRegistros => _registrosController.sink.add;

  Future<void> getRegistros() async {
    List<Registro> registros = await RegistroProvider().getRegistros();

    try {
      changeRegistros(registros);
    } catch (e) {}
  }

  dispose() {
    _registrosController?.close();
  }
}
