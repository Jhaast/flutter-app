import 'package:http/http.dart' as http;
import 'package:quick/src/models/comentarios_model.dart';

import 'dart:convert';
import 'dart:async';


class ComentarioProvider {
  String _accessToken = '8r98KCgcMygumizJR1QLxHarTY-7k2p0JrI3';
  String _url = 'https://gorest.co.in';



  Future<List<Comentario>> getComentarios(String postId) async {
    http.Response response = await http.get('$_url/public-api/comments?_format=json&access-token=$_accessToken&post_id=$postId');
    final decodedData = json.decode(response.body);
    final comentarios = new Comentarios.fromJsonList(decodedData['result']);
    return comentarios.items;
  }
}
