import 'package:http/http.dart' as http;

import 'dart:convert';
import 'dart:async';

import 'package:quick/src/models/registros_model.dart';

class RegistroProvider {
  String _accessToken = '8r98KCgcMygumizJR1QLxHarTY-7k2p0JrI3';
  String _url = 'https://gorest.co.in';



  Future<List<Registro>> getRegistros() async {
    http.Response response = await http.get('$_url/public-api/posts?_format=json&access-token=$_accessToken');
    final decodedData = json.decode(response.body);
    final registros = new Registros.fromJsonList(decodedData['result']);
    return registros.items;
  }
}
